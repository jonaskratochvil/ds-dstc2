"""
# ## Tracker classification
#
# My idea was to use the doc2vec model to train the tracker classification and use
# some classifier on top of it to correctly predict the location, price-range and area.
# This approach requires a lot of preprocessing. I have decided to merge both the user and
# machine outputs together into a single paragraph like text, each labeled with the correct tag.
#
# Through the dialogue I have kept track of the history of conversation so I have incrementally
# concatenated the dialogue turns within the dialogue in order to keep as much information as possible.
#
# The basic idea of doc2vec is to take a document (concatenation of dialogues in our case)
# and make their mathematical representation in some high dimmensional vector space. This way we
# can measure similarities between documments based on properties of these representations.
#
# I have used the libraries nltk and re for text preprocessing and gensim library for
# the doc2vec model as there is already pre-trained neural model included.
#
# The computations take quite a long time on my laptop (approx. 1 hour to run the whole code),
# therefore the results are not very great (development accuracy 31% and F-score 22.5%) as I have
# trained the doc2vec model for only 10 epochs, however, something like 50 would be much better.
# I am quite convinced that with more recources the representation by doc2vec could be valuable
# as a mean of text preprocessing for future models.
"""
# %%
# Here are all imports

import json
from pprint import pprint
from bs4 import BeautifulSoup
import re
import nltk
import nltk
from nltk.corpus import stopwords
from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from collections import defaultdict
import gensim
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, f1_score
import sys

PATH = f"/home/jonas/Jonas-zaloha/skola/Dialogove_systemy/HW3/ds-dstc2/data/dstc2"

"""
# Below are all the functions for loading the data and text-preprocessing. The final
# output are the tokenized representations of dialogues in a format to be uploaded to
# doc2vec model.
"""


def clean(text):
    text = re.sub(r"\|\|\|", r" ", text)
    text = text.lower()
    return text


def tokenize(text):
    tokens = []
    for turn in nltk.sent_tokenize(text):
        for word in nltk.word_tokenize(turn):
            if len(word) < 2:
                continue
            tokens.append(word.lower())
    return tokens


def prepare_data(data):

    with open(f"{PATH}/{data}") as f:
        data = json.load(f)

    training_data = []

    for dialogue in data:
        training_features = ""
        for i in range(len(dialogue)):

            # Keep history of dialogue through the whole conversation

            training_features += f"{dialogue[i][0]} {dialogue[i][2]} "
            training_features = clean(training_features)
            training_class = dialogue[i][4]

            training_data.append([training_features, training_class])

    return training_data


def make_tokens(data):

    tokenized = []

    for i in range(len(data)):
        tokenized.append(
            gensim.models.doc2vec.TaggedDocument(
                words=gensim.utils.simple_preprocess(data[i][0]), tags=[data[i][1]]
            )
        )

    return tokenized


# Here all the actual runs of the above functions happen for train and dev sets.


training_path = "data.dstc2.train.json"
dev_path = "data.dstc2.dev.json"

training_data = prepare_data(training_path)
dev_data = prepare_data(dev_path)

training_data = make_tokens(training_data)
dev_data = make_tokens(dev_data)

# Finally we train the doc2vec model here using the tokenized training data.

model = gensim.models.doc2vec.Doc2Vec(vector_size=50, min_count=2, epochs=10)
model.build_vocab(training_data)
model.train(training_data, total_examples=model.corpus_count,
            epochs=model.epochs)

# In this section we define the function for extracting the features from the trained model.


def vec_for_learning(model, tagged_docs):
    targ_arr = []
    indep_arr = []
    for i in range(len(tagged_docs)):
        targ_arr.append(tagged_docs[i][1])
        indep_arr.append(model.infer_vector(tagged_docs[i][0], steps=20))

    return targ_arr, indep_arr


y_train, X_train = vec_for_learning(model, training_data)
y_test, X_test = vec_for_learning(model, dev_data)

# We train the classification model here.

logreg = LogisticRegression(n_jobs=1)
logreg.fit(X_train, y_train)
y_pred = logreg.predict(X_test)

print("Accuracy is %s" % accuracy_score(y_test, y_pred))
print("F1 score is: {}".format(f1_score(y_test, y_pred, average="weighted")))


"""
# I have used quite lot of different recources in order to get the above implementation
# running as it was the first time for me to work with doc2vec model, some recources that
# I have used are listed below
# 1) https://towardsdatascience.com/multi-class-text-classification-with-doc2vec-logistic-regression-9da9947b43f4
# 2) https://medium.com/scaleabout/a-gentle-introduction-to-doc2vec-db3e8c0cce5e
# 3) https://github.com/RaRe-Technologies/gensim/blob/develop/docs/notebooks/doc2vec-lee.ipynb
# 4) https://towardsdatascience.com/multi-class-text-classification-model-comparison-and-selection-5eb066197568
# 5) https://github.com/RaRe-Technologies/gensim/blob/3c3506d51a2caf6b890de3b1b32a8b85f7566ca5/docs/notebooks/doc2vec-IMDB.ipynb
"""
