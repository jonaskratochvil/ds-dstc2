# %%
"""
I think that the biggest weakness of BLEU score is its unability to capture the meaning of a
sentence. In my example below the gold sentence is "I have finished dinner". When I calculate
the BLEU score for hypothesis sentences "I have finishe supper" "I have finished feast" and
"I have finished movie" the scores are the same for all of these hypothesis (0.75), however,
clealy the meaning of the last one is very much different form the other two hypothesis and form 
the gold one. Moreover, when we calculate the BLEU score for the gold sentence and hypothesis "I have
finished evening meal" the score is 0.6, however, I would argue that this hypothesis is the closest
one to the gold among all of them and shoud ideally receive a highest score and not the lowest one.
Example where BLEU score does a good job is that it evaluates the hypothesis "I have finished feast"
and "I have finished supper" by the same score, which is in my opinion correct as they bare pretty much the
same meaning. 
"""
from nltk.translate.bleu_score import sentence_bleu
import unittest


class MyTest(unittest.TestCase):
    def test_empty(self):
        gold = [["I", "have", "finished", "dinner"]]
        hyp0 = ["I", "have", "finished", "supper"]

        self.assertEqual(sentence_bleu(gold, hyp0, weights=(1, 0, 0, 0)), 0.75)

    def test_a_a_a(self):
        gold = [["I", "have", "finished", "dinner"]]
        hyp1 = ["I", "have", "finished", "feast"]
        self.assertEqual(sentence_bleu(gold, hyp1, weights=(1, 0, 0, 0)), 0.75)

    def test_a_b(self):
        gold = [["I", "have", "finished", "dinner"]]
        hyp2 = ["I", "have", "finished", "evening", "meal"]
        self.assertEqual(sentence_bleu(gold, hyp2, weights=(1, 0, 0, 0)), 0.6)

    def test_a_b_c_a(self):
        gold = [["I", "have", "finished", "dinner"]]
        hyp3 = ["I", "have", "finished", "movie"]
        self.assertEqual(sentence_bleu(gold, hyp3, weights=(1, 0, 0, 0)), 0.75)


if __name__ == "__main__":
    unittest.main()
